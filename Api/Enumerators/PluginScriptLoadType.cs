﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdiIRCAPIv2.Enumerators
{
    public enum PluginScriptLoadType
    {
        Starting = 0,
        Loading = 1,
        Reloading = 2
    }
}
