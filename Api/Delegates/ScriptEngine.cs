﻿namespace AdiIRCAPIv2.Delegates
{
    using Arguments.ScriptEngine;

    public delegate void EngineScriptLoad(EngineScriptLoadArgs e);

    public delegate void EngineScriptUnload(EngineScriptUnloadArgs e);
}
